<?php
if( !defined( 'ABSPATH')){ exit(); }
?>
<div class="wrap">
    <div id="rchangeplugin">
	    <div id="rch_results"></div>
        
		<!--<div class="rupdated rerror"><p>Раздел успешно отредактирован.</p></div>-->

		<div id="rchange">
		    <div class="rhead">
 			    <div class="rheadvn">
				    Импорт/Экспорт настроек
				</div>
				<div id="rajax"></div>
            </div>
			<div class="rcontent">
			    
                <div class="rblock">
	                <div class="blocktitle">Экспорт:</div>
		            <div class="rexport">
                        <a href="<?php echo RCH_PLUGIN_URL;?>ajax/export.php" class="button">Экспорт</a>
		            </div>
		            <div class="rdescription">
	                    Возможность экспортировать настройки плагина.
                    </div>
                </div>			    
				
                <div class="rblock">
	                <div class="blocktitle">Импорт:</div>
					<form action="<?php echo RCH_PLUGIN_URL;?>ajax/import.php" id="pch_import" method="post" enctype="multipart/form-data"> 
		            <div class="rexport">
                        <p><input type="file" name="importfile" accept="text/xml" value="" />
						<input type="submit" name="submit" id="goimport" class="button" value="Импортировать" />
						</p>		            
					</div>
					</form>
		            <div class="rdescription">
	                    Возможность импортировать настройки плагина.
                    </div>
                </div>				
				
			</div>
	    </div>
		
	</div>
</div>
<script type="text/javascript">
$(document).ready(function(){ 
    $('#pch_import').ajaxForm({
        beforeSubmit: function(a,f,o) {
            o.dataType = 'json';
			$('#rajax').show();
			$('#goimport').attr('disabled',true);
        },
        success: function(data) {
		    if(data['otv']==100){
			    $('#rch_results').html('<div class="rupdated"><p>'+data['text']+'.</p></div>');
			} else {
			    $('#rch_results').html('<div class="rerror"><p>Ошибка: '+data['text']+'.</p></div>');		
			} 
			$('#rajax').hide();
			$('#goimport').attr('disabled',false);
        }
    });	
});	
</script>