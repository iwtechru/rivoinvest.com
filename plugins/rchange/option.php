<?php 
if( !defined( 'ABSPATH')){ exit(); }

global $wpdb;
$razdels = $wpdb->get_results("SELECT * FROM ". $wpdb->prefix ."rchange WHERE rchto='razdel' ORDER BY rorder asc");
$toption = toption_array();
?>
<div class="wrap">
    <div id="rchangeplugin">
        <?php if($_GET['saved']=='true'){ ?><div class="rupdated"><p>Настройки успешно сохранены.</p></div><?php } ?>

		<div id="rchange">
		    <div class="rhead">
 			    <div class="rheadvn">
				    Настройки
				</div>
            </div>
			<div class="rcontent">
			    <div class="rsidebar">
				    <ul id="rtab">
					    <?php foreach($razdels as $razdel){ ?>
					    <li><a href="#rstudia-<?php echo $razdel->id;?>" class="rstudia-<?php echo $razdel->id;?>"><?php echo $razdel->rname;?></a></li>
						<?php } ?>
						<li><a href="#rstudia-0" class="rstudia-0">Админка</a></li>						
					</ul>
				</div>
				<div id="rcontent">
				<form method="post" action="">
				
                <?php foreach($razdels as $razdel){ $idrazdel=$razdel->id; ?>
				<div id="rstudia-<?php echo $idrazdel;?>" class="roptiontab">
				
				    <div class="rtitle">
					    <div class="rname"><?php echo $razdel->rname;?></div>
						<div class="rbutton"><p class="submit"><input type="submit" name="go_option" value="Сохранить" /></p></div>
					    <div class="rclear"></div>
					</div>
					
				<?php 
					if(is_array($toption[$idrazdel])){
					    foreach($toption[$idrazdel] as $the_option){
						    $smoption = unserialize($the_option->roption);
						    $rvid = $the_option->rvid;
						
						    if($rvid=='input'){
							    rblock_input($the_option->rname, $the_option->rslug , $the_option->rdefault, $the_option->rdescription);  
						    } elseif($rvid=='yesno'){
							    rblock_yesno($the_option->rname, $the_option->rslug , $the_option->rdescription);
							} elseif($rvid=='radio'){
							    rblock_radio($the_option->rname, $the_option->rslug , $smoption, $the_option->rdefault, $the_option->rdescription); 
                            } elseif($rvid=='checkbox'){
							    rblock_checkbox($the_option->rname, $the_option->rslug , $smoption, $the_option->rdefault, $the_option->rdescription);  								
                            } elseif($rvid=='select'){
							    rblock_select($the_option->rname, $the_option->rslug , $smoption, $the_option->rdefault, $the_option->rdescription);							
							} elseif($rvid=='upload'){
							    rblock_upload($the_option->rname, $the_option->rslug , $the_option->rdefault, $the_option->rdescription);
							} elseif($rvid=='color'){
							    rblock_color($the_option->rname, $the_option->rslug , $the_option->rdefault, $the_option->rdescription);
							} elseif($rvid=='cdate'){
							    rblock_cdate($the_option->rname, $the_option->rslug , $the_option->rdefault, $the_option->rdescription);
							} elseif($rvid=='cdatetime'){
							    rblock_cdatetime($the_option->rname, $the_option->rslug , $the_option->rdefault, $the_option->rdescription);								
							} elseif($rvid=='textarea'){			
                                rblock_textarea($the_option->rname, $the_option->rslug , $the_option->rdefault, $the_option->rdescription);
							} elseif($rvid=='iduser'){
							    if(!is_array($rch_users)){
								    $rch_users = $wpdb->get_results("SELECT ID, user_login FROM ". $wpdb->prefix ."users ORDER BY user_login ASC");
								}
							    $narray = array();
								if(is_array($rch_users)){ 
								    foreach($rch_users as $rchus){
								        $narray[] = $rchus->ID .'*|*'.$rchus->user_login;
								    }
								}
								rblock_select($the_option->rname, $the_option->rslug , $narray, $the_option->rdefault, $the_option->rdescription);
							} elseif($rvid=='loginuser'){
							    if(!is_array($rch_users)){
								    $rch_users = $wpdb->get_results("SELECT ID, user_login FROM ". $wpdb->prefix ."users ORDER BY user_login ASC");
								}
							    $narray = array();
								if(is_array($rch_users)){ 
								    foreach($rch_users as $rchus){
								        $narray[] = $rchus->user_login .'*|*'.$rchus->user_login;
								    }
								}
								rblock_select($the_option->rname, $the_option->rslug , $narray, $the_option->rdefault, $the_option->rdescription);							
							} elseif(preg_match("/^ptid_/", $rvid)){
							    if(!is_array($rchposttype[$rvid])){
								    $ptype = str_replace('ptid_','',$rvid);
								    $rchposttype[$rvid] = $wpdb->get_results("SELECT ID, post_title FROM ". $wpdb->prefix ."posts WHERE post_status='publish' AND post_type='$ptype' ORDER BY ID ASC");
								}
								$narray = array();
								if(is_array($rchposttype[$rvid])){ 
								    foreach($rchposttype[$rvid] as $rchpt){
								        $narray[] = $rchpt->ID .'*|*'.$rchpt->post_title;
								    }
								}	
                                rblock_select($the_option->rname, $the_option->rslug , $narray, $the_option->rdefault, $the_option->rdescription);								
							} elseif(preg_match("/^pturl_/", $rvid)){
							    if(!is_array($rchposturl[$rvid])){
								    $ptype = str_replace('pturl_','',$rvid);
								    $rchposturl[$rvid] = $wpdb->get_results("SELECT ID, post_title FROM ". $wpdb->prefix ."posts WHERE post_status='publish' AND post_type='$ptype' ORDER BY ID ASC");
								}
								$narray = array();
								if(is_array($rchposturl[$rvid])){ 
								    foreach($rchposturl[$rvid] as $rchpt){
								        $narray[] = get_permalink($rchpt->ID) .'*|*'.$rchpt->post_title;
								    }
								}	
                                rblock_select($the_option->rname, $the_option->rslug , $narray, $the_option->rdefault, $the_option->rdescription);								
							} elseif(preg_match("/ttid_/", $rvid)){
							    if(!is_array($rchtax[$rvid])){
								    $ptype = str_replace('ttid_','',$rvid);
								    $rchtax[$rvid] = get_terms( $ptype, 'orderby=name&order=asc&hide_empty=0');
								}
								$narray = array();
								if(is_array($rchtax[$rvid])){ 
								    foreach($rchtax[$rvid] as $rchpt){
								        $narray[] = $rchpt->term_id .'*|*'.$rchpt->name;
								    }
								}	
                                rblock_select($the_option->rname, $the_option->rslug , $narray, $the_option->rdefault, $the_option->rdescription);								
																						
							
							
							
							}	
							
							
					    }
					}
				?>				
					
				    <div class="rtitle rbottom">
						<div class="rbutton"><p class="submit"><input type="submit" name="go_option" value="Сохранить" /></p></div>
					    <div class="rclear"></div>
					</div>	

                </div>
				<?php } ?>	
					
					
					
					
				
				    <div id="rstudia-0" class="roptiontab">
				
				    <div class="rtitle">
					    <div class="rname">Настройки админки</div>
						<div class="rbutton"><p class="submit"><input type="submit" name="go_option" value="Сохранить" /></p></div>
					    <div class="rclear"></div>
					</div>

                    <?php 
					rblock_radio('Показывать ссылки в футере?', 'back1', array('true*|*Да','false*|*Нет'), 'true');
					rblock_radio('Показывать слово "Привет" перед именем?', 'back2', array('true*|*Да','false*|*Нет'), 'true');
					?>
					<div class="hrrazdel"></div>
					<?php
					rblock_radio('Показывать виджет "Прямо сейчас"?', 'back_widget1', array('true*|*Да','false*|*Нет'), 'true');
                    rblock_radio('Показывать виджет "Свежие комментарии"?', 'back_widget2',array('true*|*Да','false*|*Нет'), 'true');
					rblock_radio('Показывать виджет "Входящие ссылки"?', 'back_widget3', array('true*|*Да','false*|*Нет'), 'true');
					rblock_radio('Показывать виджет "Блог Wordpress"?', 'back_widget4', array('true*|*Да','false*|*Нет'), 'true');
					rblock_radio('Показывать виджет "Другие новости WordPress"?', 'back_widget5', array('true*|*Да','false*|*Нет'), 'true');
					rblock_radio('Показывать виджет "Плагины"?', 'back_widget6', array('true*|*Да','false*|*Нет'), 'true');
					rblock_radio('Показывать виджет "Быстрая публикация"?', 'back_widget7', array('true*|*Да','false*|*Нет'), 'true');
					rblock_radio('Показывать виджет "Свежие черновики"?', 'back_widget8', array('true*|*Да','false*|*Нет'), 'true');
					?>
					<div class="hrrazdel"></div>
					<?php
					rblock_radio('Показывать раздел меню "Консоль"?', 'back_menu1', array('true*|*Да','false*|*Нет'), 'true');
					rblock_radio('Показывать раздел меню "Записи"?', 'back_menu2', array('true*|*Да','false*|*Нет'), 'true');
					rblock_radio('Показывать раздел меню "Медиафайлы"?', 'back_menu3', array('true*|*Да','false*|*Нет'), 'true');
					rblock_radio('Показывать раздел меню "Ссылки"?', 'back_menu4', array('true*|*Да','false*|*Нет'), 'true');
					rblock_radio('Показывать раздел меню "Страницы"?', 'back_menu5', array('true*|*Да','false*|*Нет'), 'true');
					rblock_radio('Показывать раздел меню "Комментарии"?', 'back_menu6', array('true*|*Да','false*|*Нет'), 'true');
					rblock_radio('Показывать раздел меню "Плагины"?', 'back_menu7', array('true*|*Да','false*|*Нет'), 'true');
					rblock_radio('Показывать раздел меню "Пользователи"?', 'back_menu8', array('true*|*Да','false*|*Нет'), 'true');
					rblock_radio('Показывать раздел меню "Инструменты"?', 'back_menu9', array('true*|*Да','false*|*Нет'), 'true');
					rblock_radio('Показывать раздел меню "Параметры"?', 'back_menu10', array('true*|*Да','false*|*Нет'), 'true');
					
					?>
					
					
				    <div class="rtitle rbottom">
						<div class="rbutton"><p class="submit"><input type="submit" name="go_option" value="Сохранить" /></p></div>
					    <div class="rclear"></div>
					</div>	

					</form>
                    </div>
					
				</div>
			    <div class="rclear"></div>
			</div>
	    </div>
		
	</div>
</div>