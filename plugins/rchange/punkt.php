<?php
if( !defined( 'ABSPATH')){ exit(); }
$opt = intval($_GET['optional']);
$num = intval($_GET['n']);
$idrazdel = intval($_GET['razdel']);
if(!$opt){$opt=1;}

global $wpdb;
$razdels = $wpdb->get_results("SELECT * FROM ".$wpdb->prefix."rchange WHERE rchto='razdel' ORDER BY rorder asc");
$options = $wpdb->get_results("SELECT * FROM ".$wpdb->prefix."rchange WHERE rchto='options' AND toid='$idrazdel' ORDER BY rorder asc");
?>
<div class="wrap">
    <div id="rchangeplugin">
        <?php if($_GET['saved']=='true'){ ?><div class="rupdated"><p>Опция успешно добавлена.</p></div><?php } ?>
		<?php if($_GET['saved']=='edittrue'){ ?><div class="rupdated"><p>Опция успешно отредактирована.</p></div><?php } ?>
		<?php if($_GET['saved']=='delete'){ ?><div class="rerror"><p>Опция удалена.</p></div><?php } ?>
		<?php if($_GET['saved']=='noslug'){ ?><div class="rerror"><p>Данный slug уже используется.</p></div><?php } ?>
	

		<div id="rchange">
		    <div class="rhead">
 			    <div class="rheadvn">
				    Опции разделов
				</div>
				<div id="rajax"></div>
            </div>
			<div class="rcontent">
			    <div class="rsidebar rright">
				    <div class="rdelayvibor">
                        <select name="" onchange="location = this.options[this.selectedIndex].value;">
						    <option value="<?php echo admin_url('admin.php?page=rchange/punkt.php&razdel=0');?>">Без раздела</option>
						    <?php 
							foreach($razdels as $razdel){ 
							$razdel_id = $razdel->id;
							?>
							<option value="<?php echo admin_url('admin.php?page=rchange/punkt.php&razdel='.$razdel_id);?>" <?php if($razdel_id==$idrazdel){?>selected="selected"<?php } ?>><?php echo $razdel->rname;?></option>
							<?php } ?>
						</select>					
					</div>
				    <ul id="rgoed">
                        <?php 
						foreach($options as $oneo){ 
						$one = $oneo->id;
						$slug = $oneo->rslug;
						$rnid = $oneo->toid;
						?>
					    <li id="number_<?php echo $one; ?>">
						    <div class="rliname"><a href="<?php echo admin_url('admin.php?page=rchange/punkt.php&razdel='.$rnid.'&optional=2&n='.$one);?>"><?php echo $oneo->rname;?></a></div> 
							<div class="rlikey"><input type="text" onclick="this.select()" name="" value="<?php echo $slug;?>" /></div>
							
							<div class="rliconf">
							    <div class="rlimess"><a href="<?php echo admin_url('admin.php?page=rchange/punkt.php&razdel='.$rnid.'&optional=3&n='.$one);?>">Да</a> или <a href="#" class="nodel">нет</a> ?</div>
							    <a href="<?php echo admin_url('admin.php?page=rchange/punkt.php&razdel='.$rnid.'&optional=2&n='.$one);?>">Редактировать</a> | <a href="#" class="rdel">Удалить</a>
							</div>
						    
						</li>
                        <?php } ?>
					</ul>
				</div>
				<div id="rcontent" class="rleft">
				    <div class="rtitle">
					    <div class="rname">
						<?php if($opt==2){?>
                            Редактировать опцию <a href="<?php echo admin_url('admin.php?page=rchange/punkt.php&razdel='.$idrazdel);?>">Добавить новую</a>
                        <?php } else {?>
                            Добавить новую опцию
                        <?php }?>
						</div>
					    <div class="rclear"></div>
					</div>
					
					<form method="post" action="<?php if($opt==2){?><?php echo admin_url('admin.php?page=rchange/punkt.php&optional=2&n='.$num);?><?php } else {?><?php echo admin_url('admin.php?page=rchange/punkt.php');?><?php } ?>">
					
					<?php 
					if($opt==2 and $num){
                    global $wpdb;
                    $field = $wpdb->get_row("SELECT * FROM ".$wpdb->prefix."rchange WHERE id='$num' AND rchto='options'");
                    }                   
                    ?>
					
					<div class="rblock">
					    <div class="blocktitle">Название</div>
						<div class="rinput">
                            <input type="text" class="rlong" name="rname" value="<?php echo $field->rname;?>" />
					    </div>
						<div class="rdescription">
						Название вашей опции (обязательное поле)
						</div>
					</div>

					<div class="rblock">
					    <div class="blocktitle">Slug</div>
						<div class="rinput">
                            <input type="text" class="rlong" name="rslug" value="<?php echo $field->rslug;?>" />
					    </div>
						<div class="rdescription">
						Псевдоним опции (обязательное поле)
						</div>
					</div>						
					
					<div class="rblock">
					    <div class="blocktitle">К разделу настроек</div>
						<div class="rselect">
                            <select name="toid">
						        <option value="0">Без раздела</option>
						        <?php 
								if($opt==2){
								   $tid = $field->toid;
								} else {
								   $tid = $idrazdel;
								}
								
							    foreach($razdels as $razdel){ 
							    $razdel_id = $razdel->id;
							    ?>
							    <option value="<?php echo $razdel_id;?>" <?php if($razdel_id==$tid){?>selected="selected"<?php } ?>><?php echo $razdel->rname;?></option>
							    <?php } ?>
						    </select>
					    </div>
						<div class="rdescription">
						Выберите к какому разделу будет принадлежать опция.<br /> 
						<strong>"Без раздела"</strong> не участвует в настройках.
						</div>						
					</div>

					<div class="rblock">
					    <div class="blocktitle">Подсказка</div>
						<div class="rtextarea">
                            <textarea name="rdescription" style="height: 100px;"><?php echo $field->rdescription;?></textarea>
					    </div>
						<div class="rdescription">
						Введите подсказку для поля
						</div>						
					</div>					
					
					<div class="rblock">
					<?php $firvi = $field->rvid; ?>
					    <div class="blocktitle">Вид</div>
						<div class="rselect">
                            <select name="rvid">
						        <option value="input" <?php if('input'==$firvi){?>selected="selected"<?php } ?>>Поле ввода</option>
								<option value="yesno" <?php if('yesno'==$firvi){?>selected="selected"<?php } ?>>Да / Нет</option>
								<option value="cdate" <?php if('cdate'==$firvi){?>selected="selected"<?php } ?>>Дата</option>
								<option value="cdatetime" <?php if('cdatetime'==$firvi){?>selected="selected"<?php } ?>>Дата и время</option>
                                <option value="select" <?php if('select'==$firvi){?>selected="selected"<?php } ?>>Выпадающий список</option>
								<option value="checkbox" <?php if('checkbox'==$firvi){?>selected="selected"<?php } ?>>Checkbox</option>
								<option value="radio" <?php if('radio'==$firvi){?>selected="selected"<?php } ?>>Radio</option>
								<option value="upload" <?php if('upload'==$firvi){?>selected="selected"<?php } ?>>Загрузка</option>
								<option value="color" <?php if('color'==$firvi){?>selected="selected"<?php } ?>>Цвет</option>
								<option value="textarea" <?php if('textarea'==$firvi){?>selected="selected"<?php } ?>>Текстовое поле</option>
								<option value="iduser" <?php if('iduser'==$firvi){?>selected="selected"<?php } ?>>ID пользователя</option>
								<option value="loginuser" <?php if('loginuser'==$firvi){?>selected="selected"<?php } ?>>Имя пользователя</option>
								<?php  
								$args=array('public' => true );
								$post_types=get_post_types($args,'names');
								foreach($post_types as $pt){
								     if($pt != 'attachment'){
									 $obj = get_post_type_object($pt);
									 $ptn = $obj->labels->singular_name;
								?>
								<option value="ptid_<?php echo $pt;?>" <?php if('ptid_'.$pt==$firvi){?>selected="selected"<?php } ?>>ID <?php echo $ptn;?></option>
								<option value="pturl_<?php echo $pt;?>" <?php if('pturl_'.$pt==$firvi){?>selected="selected"<?php } ?>>URL <?php echo $ptn;?></option>
								<?php }
								} 
								$taxonomies=get_taxonomies('','objects');
								foreach($taxonomies as $rtax){
								    $not = array('nav_menu','link_category','post_format');
									$name = $rtax->name;
								    if(!in_array($name,$not)){
	
								?>
								<option value="ttid_<?php echo $name;?>" <?php if('ttid_'.$name==$firvi){?>selected="selected"<?php } ?>>ID <?php echo $rtax->labels->name;?></option>
								<?php }	}							
								?>
						    </select>
					    </div>
						<div class="rdescription">
						Выберите вид опции.
						</div>						
					</div>					

					<div class="rblock myvariant" <?php if($firvi=='select' or $firvi=='checkbox' or $firvi=='radio'){ ?>style="display: block;"<?php } ?>>
					    <div class="blocktitle">Варианты</div>
						<div class="rvariant">
						    <?php
							$ropt = unserialize($field->roption);
							if(is_array($ropt)){
							foreach($ropt as $ros){
							$ro = explode('*|*',$ros);
							?>
							<p><input type="text" class="rvaria" name="roption[]" value="<?php echo $ro[0];?>" /> => <input type="text" class="rvaria" name="roptionv[]" value="<?php echo $ro[1];?>" /> <a href="#" class="ach rtmin">[ - ]</a> <a href="#" class="ach rtplu">[ + ]</a></p>
							<?php }} ?>
                            <p><input type="text" class="rvaria" name="roption[]" value="" /> => <input type="text" class="rvaria" name="roptionv[]" value="" /> <a href="#" class="ach rtmin">[ - ]</a> <a href="#" class="ach rtplu">[ + ]</a></p> 
							
					    </div>
					</div>					
					
					
					<div class="rblock">
					    <div class="blocktitle">Значение по умолчанию</div>
						<div class="rinput">
                            <input type="text" class="rlong" name="rdefault" value="<?php echo $field->rdefault;?>" />
					    </div>
						<div class="rdescription">
						Если поле настроек не заполнено, будет выводится это значение.
						</div>
					</div>					
					
					
				    <div class="rtitle rbottom">
						<div class="rbutton"><p class="submit"><input type="submit" name="" value="Сохранить" /></p></div>
					    <div class="rclear"></div>
					</div>	
                    <?php if($opt==1 or $opt==3){ ?>
                        <input type="hidden" name="action" value="1" />
                    <?php } elseif($opt==2){ ?>
                        <input type="hidden" name="action" value="2" />
                    <?php } ?>	
                    </form>
					
				</div>
			    <div class="rclear"></div>
			</div>
	    </div>
		
	</div>
</div>
<script type="text/javascript">
$(document).ready(function(){ 
    $('select[name=rvid]').change(function(){
	    var valu = $(this).val();
	    if(valu=='select' || valu=='checkbox' || valu=='radio'){
		    $('.myvariant').show();
		} else {
	        $('.myvariant').hide();
	    }
	});
	$('.rtplu').live('click',function(){
	    $(this).parents('p').after('<p><input type="text" class="rvaria" name="roption[]" value="" /> => <input type="text" class="rvaria" name="roptionv[]" value="" /> <a href="#" class="ach rtmin">[ - ]</a> <a href="#" class="ach rtplu">[ + ]</a></p>');
	return false;
	});
	$('.rtmin').live('click',function(){
	    var kolvo = $(this).parents('.rvariant').find('p').length;
		if(kolvo > 1){
		    $(this).parents('p').remove();
		}
	return false;
	});	
$("#rgoed").sortable({ 
	opacity: 0.6, 
	cursor: 'move',
	revert: true,
 	update: function() {
		    $('#rajax').show();
			var order = $(this).sortable("serialize"); 
			$.post("<?php echo RCH_PLUGIN_URL;?>/ajax/razdel_admin_drag.php", order, 
			
			function(theResponse){
				$('#rajax').hide();
			}); 															 
	}	 	
});
$('#rgoed li').hover(function(){
    $(this).find('.rliconf').stop(true,true).slideDown(300);
}, function(){
    $(this).find('.rliconf').stop(true,true).slideUp(300);
	$(this).find('.rlimess').hide();
});

$('.rdel').click(function(){
    $(this).parents('li').find('.rlimess').show();
return false;
});

$('.nodel').click(function(){
    $(this).parents('li').find('.rlimess').hide();
return false;
});	
	
});	
</script>