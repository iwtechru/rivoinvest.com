<?php
/*
Plugin Name: Rchange
Plugin URI: http://rstudia.ru/myplugins/rchange/
Description: Настройки Wordpress от Rstudia.
Version: 0.9
Author: Юрий Richkeeper Андреев
Author URI: http://rstudia.ru/
*/

if( !defined( 'ABSPATH')){ exit(); }

if(!defined('RCH_PLUGIN_NAME')){
	define('RCH_PLUGIN_NAME', plugin_basename(__FILE__));
}
if(!defined('RCH_PLUGIN_DIR')){
	define('RCH_PLUGIN_DIR', dirname(__FILE__).'/');
}
if(!defined('RCH_PLUGIN_URL')){
	define('RCH_PLUGIN_URL', plugins_url(str_replace('.php','',basename(RCH_PLUGIN_NAME))).'/');
}

function rchange_default(){
global $wpdb;
$narr = array();
$rch_def = $wpdb->get_results("SELECT rslug, rdefault FROM ". $wpdb->prefix ."rchange WHERE rchto='options' AND toid != '0'");
foreach($rch_def as $rd){
    $narr[$rd->rslug] = $rd->rdefault; 
}
return $narr;
}

global $rchange_option;
$rchange_option = get_option('rchange_option');

global $rchange_default;
$rchange_default = rchange_default();

function rch_template($page){
$pager = RCH_PLUGIN_DIR . "/".$page.".php";
    if(file_exists($pager)){
        include($pager);
    }
}

rch_template('functions');
rch_template('backadmin');

add_action('admin_menu', 'rch_add_admin');
function rch_add_admin() {
	add_menu_page("Rchange - настройки сайта", "Настройки", 10, 'rchange/option.php', 'rch_admin', RCH_PLUGIN_URL .'/images/rlink.png');  
    add_submenu_page("rchange/option.php", "Разделы", "Разделы", 10, "rchange/razdel.php", "rch_razdel");
	add_submenu_page("rchange/option.php", "Опции", "Опции", 10, "rchange/punkt.php", "rch_punkt");
	add_submenu_page("rchange/option.php", "Импорт/Экспорт", "Импорт/Экспорт", 10, "rchange/import.php", "rch_import");
}

function rch_admin() {
    rch_template('option');
}

function rch_import() {
    rch_template('import');
}

function rch_razdel() {
    rch_template('razdel');
}

function rch_punkt() {
    rch_template('punkt');
}

add_action('admin_init', 'rch_add_init');
function rch_add_init() {
$file_dir = RCH_PLUGIN_URL;

    if (preg_match('/^rchange/i',$_GET['page'])){	
        wp_enqueue_style('rchange style', $file_dir."style.css", false, "1.0");
		wp_enqueue_style('rchange ui style', $file_dir."jquery-ui-1.8.21.custom.css", false, "1.0");
        wp_deregister_script('jquery');
        wp_register_script('jquery', $file_dir.'js/jquery-1.7.2.min.js', false, '1.7.2');
        wp_enqueue_script('jquery');	
        wp_enqueue_script("jquery-ui", $file_dir."js/jquery-ui-1.8.21.custom.min.js", false, "1.0");		
	}
	if($_GET['page']=='rchange/option.php'){
	    wp_enqueue_script("jquery-colorpicker", $file_dir."js/colorpicker.js", false, "1.0");
	    wp_enqueue_script("jquery-cookie", $file_dir."js/jquery.cookie.js", false, "1.0");
        wp_enqueue_script("roption script", $file_dir."js/script.js", false, "1.0");
		wp_enqueue_script("jquery-date", $file_dir."js/jquery.ui.datepicker-ru.js", false, "1.0");
        wp_enqueue_script("jquery-time", $file_dir."js/jquery-ui-timepicker-addon.js", false, "1.0");
        wp_enqueue_script('media-upload');
        wp_enqueue_script('thickbox');
        wp_enqueue_style('thickbox');		
	}
	if($_GET['page']=='rchange/import.php'){	
        wp_enqueue_script("jquery-pch-form", $file_dir.'js/jquery.form.js', false, '2.94');	
	}	
	
if (current_user_can('administrator')){	
    if ( $_GET['page'] == 'rchange/option.php' ) {
        if ($_POST['go_option']) {
		        $roption = $_POST;
		        unset($roption['go_option']);
				$noption = array();
                foreach ($roption as $key => $value) {
				    if(is_array($value)){
					    $noption[$key] = $value;
					} else {
					    $noption[$key] = rch_del_quoter($value);
					}
				}
				update_option( 'rchange_option', $noption ); 
                header("Location: admin.php?page=rchange/option.php&saved=true");
                die;
        }
    }
	if($_GET['page']=='rchange/razdel.php'){
	    $num = intval($_GET['n']);
		$opt = intval($_GET['optional']);
		$rname = rch_del_quote(trim(stripslashes(strip_tags($_POST['rname']))));
		global $wpdb;
        if($_POST['action']==1 and $rname){ 
            $wpdb->insert( $wpdb->prefix.'rchange' , array('rname' => $rname, 'rchto' => 'razdel'));
            $id = $wpdb->insert_id;
			header("Location: admin.php?page=rchange/razdel.php&n=$id&optional=2&saved=true");
			die;
			
        } elseif($_POST['action']==2 and $num and $rname){
        $wpdb->query("UPDATE ".$wpdb->prefix."rchange SET rname ='$rname' WHERE id = '$num' AND rchto='razdel'");
            header("Location: admin.php?page=rchange/razdel.php&n=$num&optional=2&saved=edittrue");
			die;
        } elseif($opt==3 and $num){
        $wpdb->query("DELETE FROM ".$wpdb->prefix."rchange WHERE id = '$num'");
		$wpdb->query("UPDATE ".$wpdb->prefix."rchange SET toid = '0' WHERE toid = '$num' AND rchto='options'");
            header("Location: admin.php?page=rchange/razdel.php&saved=delete");
			die;		
        } 	
	}
	if($_GET['page']=='rchange/punkt.php'){
	    $num = intval($_GET['n']);
		$opt = intval($_GET['optional']);
		$rname = rch_del_quote(trim(stripslashes(strip_tags($_POST['rname']))));
		$toid = intval($_POST['toid']);
		$razdel = intval($_GET['razdel']);
		$rtextarea = rch_del_quote(trim(stripslashes($_POST['rtextarea'])));
		$rvid = rch_del_quote(trim(stripslashes(strip_tags($_POST['rvid']))));
		$rslug = rch_del_quote(trim(stripslashes(strip_tags($_POST['rslug']))));
		$rslug = str_replace('back','',$rslug);
		$noption = $_POST['roption'];
		$noptionv = $_POST['roptionv'];
		$roption = array();
		if(is_array($noption)){
		    foreach($noption as $key => $value){
			    $value = rch_del_quote(trim(stripslashes(strip_tags($value))));
			    if(strlen($value) > 0){
			    $roption[]=$value.'*|*'.rch_del_quote(trim(stripslashes(strip_tags($noptionv[$key]))));
		        }
			}
		}
		$roption = serialize($roption);
		$rdescription = rch_del_quote(trim(stripslashes($_POST['rdescription'])));
		$rdefault = rch_del_quote(trim(stripslashes(strip_tags($_POST['rdefault']))));
		global $wpdb;
        if($_POST['action']==1 and $rname and $rslug){
            $countslug = $wpdb->query("SELECT id FROM ". $wpdb->prefix ."rchange WHERE rslug = '$rslug'");		
            if($countslug > 0){
			header("Location: admin.php?page=rchange/punkt.php&razdel=$toid&saved=noslug");
			exit;
			}
			$wpdb->insert( $wpdb->prefix.'rchange' , array('rname' => $rname, 'roption' => $roption, 'rdescription' => $rdescription, 'rslug'=> $rslug, 'rdefault' => $rdefault, 'rvid' => $rvid, 'toid' => $toid, 'rchto' => 'options'));
            $id = $wpdb->insert_id;
			header("Location: admin.php?page=rchange/punkt.php&n=$id&razdel=$toid&optional=2&saved=true");
			die;
        } elseif($_POST['action']==2 and $num and $rname and $rslug){
        $wpdb->query("UPDATE ".$wpdb->prefix."rchange SET rname = '$rname', roption = '$roption', rdescription = '$rdescription', rslug='$rslug', rdefault = '$rdefault', rvid='$rvid', toid = '$toid' WHERE id = '$num' AND rchto='options'");
            header("Location: admin.php?page=rchange/punkt.php&n=$num&razdel=$toid&optional=2&saved=edittrue");
			die;
        } elseif($opt==3 and $num){
        $wpdb->query("DELETE FROM ".$wpdb->prefix."rchange WHERE id = '$num'");
            header("Location: admin.php?page=rchange/punkt.php&razdel=$razdel&saved=delete");
			die;		
        } 	
	}
}
	
}

add_action('activate_'. RCH_PLUGIN_NAME,'rch_plugins_activate');
function rch_plugins_activate(){
    global $wpdb;
    $sql = "CREATE TABLE IF NOT EXISTS `". $wpdb->prefix ."rchange`(
		`id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
		`rname` varchar(255) NOT NULL,
		`rvid` varchar(250) NOT NULL default 'input',
		`roption` longtext NOT NULL,
		`rdescription` longtext NOT NULL,
		`rdefault` varchar(250) NOT NULL,
		`rslug` longtext NOT NULL,
		`rchto` varchar(250) NOT NULL default 'razdel',
		`toid` bigint(20) NOT NULL default '0',
		`rorder` bigint(20) NOT NULL default '0',
		PRIMARY KEY ( `id` )	
	) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=1;";
	$wpdb->query($sql);	

	$query = $wpdb->query("SHOW COLUMNS FROM ". $wpdb->prefix ."rchange LIKE 'rslug'");
    if ($query == 0) {
     $wpdb->query("ALTER TABLE ". $wpdb->prefix ."rchange ADD `rslug` longtext NOT NULL");
    }	
	
}
register_uninstall_hook( __FILE__, 'rch_plugins_uninstall');
function rch_plugins_uninstall(){
    global $wpdb;
	$wpdb->query("DROP TABLE " . $wpdb->prefix . "rchange");
	delete_option('rchange_option');
}
?>