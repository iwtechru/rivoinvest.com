<?php
if( !defined( 'ABSPATH')){ exit(); }

$opt = intval($_GET['optional']);
$num = intval($_GET['n']);
if(!$opt){$opt=1;}

global $wpdb;
$razdels = $wpdb->get_results("SELECT * FROM ".$wpdb->prefix."rchange WHERE rchto='razdel' ORDER BY rorder asc");
?>
<div class="wrap">
    <div id="rchangeplugin">
        <?php if($_GET['saved']=='true'){ ?><div class="rupdated"><p>Раздел успешно добавлен.</p></div><?php } ?>
		<?php if($_GET['saved']=='edittrue'){ ?><div class="rupdated"><p>Раздел успешно отредактирован.</p></div><?php } ?>
		<?php if($_GET['saved']=='delete'){ ?><div class="rerror"><p>Раздел удален.</p></div><?php } ?>

		<div id="rchange">
		    <div class="rhead">
 			    <div class="rheadvn">
				    Разделы настроек
				</div>
				<div id="rajax"></div>
            </div>
			<div class="rcontent">
			    <div class="rsidebar rright">
				    <ul id="rgoed">
					    <?php foreach($razdels as $razdel){ 
						$razdel_id = $razdel->id;
						?>
					    <li id="number_<?php echo $razdel_id; ?>">
						    <div class="rliname"><a href="<?php echo admin_url('admin.php?page=rchange/razdel.php&optional=2&n='.$razdel_id);?>"><?php echo $razdel->rname;?></a></div> 
							<div class="rliconf">
							    <div class="rlimess"><a href="<?php echo admin_url('admin.php?page=rchange/razdel.php&optional=3&n='.$razdel_id);?>">Да</a> или <a href="#" class="nodel">Нет</a>?</div>
							    <a href="<?php echo admin_url('admin.php?page=rchange/razdel.php&optional=2&n='.$razdel_id);?>">Редактировать</a> | <a href="#" class="rdel">Удалить</a>
							</div>
						</li>
						<?php } ?>
					</ul>
				</div>
				<div id="rcontent" class="rleft">
				    <div class="rtitle">
					    <div class="rname">
						<?php if($opt==2){?>
                            Редактировать раздел <a href="<?php echo admin_url('admin.php?page=rchange/razdel.php');?>">Добавить новый</a>
                        <?php } else {?>
                            Добавить новый раздел
                        <?php }?>
						</div>
					    <div class="rclear"></div>
					</div>
					<form method="post" action="<?php if($opt==2){?><?php echo admin_url('admin.php?page=rchange/razdel.php&optional=2&n='.$num);?><?php } else {?><?php echo admin_url('admin.php?page=rchange/razdel.php');?><?php } ?>">
					
					<?php if($opt==2 and $num){
                    global $wpdb;
                    $field = $wpdb->get_row("SELECT * FROM ".$wpdb->prefix."rchange WHERE id='$num' AND rchto='razdel'");
                    }                   
                    ?>
					
					<div class="rblock">
					    <div class="blocktitle">Название раздела</div>
						<div class="rinput">
                            <input type="text" class="rlong" name="rname" value="<?php echo $field->rname;?>" />
					    </div>
					</div>				
					
				    <div class="rtitle rbottom">
						<div class="rbutton"><p class="submit"><input type="submit" name="" value="Сохранить" /></p></div>
					    <div class="rclear"></div>
					</div>	
                    <?php if($opt==1 or $opt==3){ ?>
                        <input type="hidden" name="action" value="1" />
                    <?php } elseif($opt==2){ ?>
                        <input type="hidden" name="action" value="2" />
                    <?php } ?>					

                    </form>					
				</div>
			    <div class="rclear"></div>
			</div>
	    </div>
		
	</div>
</div>
<script type="text/javascript">
$(document).ready(function(){ 

$("#rgoed").sortable({ 
	opacity: 0.6, 
	cursor: 'move',
	revert: true,
 	update: function() {
		    $('#rajax').show();
			var order = $(this).sortable("serialize"); 
			$.post("<?php echo RCH_PLUGIN_URL;?>/ajax/razdel_admin_drag.php", order, 
			
			function(theResponse){
				$('#rajax').hide();
			}); 															 
	}	 	
});

$('#rgoed li').hover(function(){
    $(this).find('.rliconf').stop(true,true).slideDown(300);
}, function(){
    $(this).find('.rliconf').stop(true,true).slideUp(300);
	$(this).find('.rlimess').hide();
});

$('.rdel').click(function(){
    $(this).parents('li').find('.rlimess').show();
return false;
});

$('.nodel').click(function(){
    $(this).parents('li').find('.rlimess').hide();
return false;
});


});	
</script>