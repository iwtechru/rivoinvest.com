jQuery(function(){
    var roption = $.cookie("roption");
	var rlastpage = $.cookie("rlastpage");
	var tabContainers = jQuery('.roptiontab');
	if(roption !== '1'){
	jQuery('#rtab li:last').addClass('act');
	jQuery('.roptiontab:last').show();
	} else {
	jQuery('.'+rlastpage).parents('li').addClass('act');
	jQuery('#'+rlastpage).show();
	}
    jQuery('#rtab li a').click(function () {
	    $.cookie("roption", '1');
		var newindex = $(this).attr('href').replace('#','');
		$.cookie("rlastpage", newindex);
        tabContainers.hide().filter(this.hash).fadeIn(500);
        jQuery('#rtab li').removeClass('act');
        jQuery(this).parents('li').addClass('act');
        return false;
    });
	
	var txtBox_id = '';
	jQuery('.upload_image_button').click(function() {
		txtBox_id = '#'+jQuery(this).attr('id').replace('_button', '');
		formfield = jQuery(txtBox_id);
		tb_show('', 'media-upload.php?type=image&amp;TB_iframe=true');
		return false;
	});
	window.send_to_editor = function(html) {
		imgurl = html.replace(/"+/g,"'");
		imgurl = imgurl.split("<a href='");
		imgurl = imgurl[1].split("'");
		imgurl = imgurl[0];
		jQuery(txtBox_id).val(imgurl);
		tb_remove();
	}

    jQuery('.rthecolor').ColorPicker({
	onSubmit: function(hsb, hex, rgb, el) {
	    jQuery(el).val('#'+hex);
		jQuery(el).parents('.rblock').find('.primercolor').css({'background': '#'+hex});
		jQuery(el).ColorPickerHide();
	},
	onBeforeShow: function () {
		jQuery(this).ColorPickerSetColor(this.value);
	}
    })	
	
    jQuery('.rcdate').datepicker({
	changeYear: true
	});

    jQuery('.rcdatetime').datetimepicker({ 
	timeFormat: 'h:m:00',
	separator: ' '
    });		
	
});