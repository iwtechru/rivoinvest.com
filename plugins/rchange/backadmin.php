<?php
if( !defined( 'ABSPATH')){ exit(); }

	

if(get_roption('back1') == 'false'){
add_filter( 'admin_footer_text', '__return_false' );
}


if(get_roption('back2') == 'false'){
add_filter( 'gettext', 'rch_remove_howdy', 10, 3 );
function rch_remove_howdy( $translation, $text, $domain ) {
if ( $text == 'Howdy, %1$s' )
	return '%1$s';
	return $translation;
}
}

function rch_remove_dashboard_widgets() {
global $wp_meta_boxes;

if(get_roption('back_widget1')=='false'){
unset($wp_meta_boxes['dashboard']['normal']['core']['dashboard_right_now']);
}
if(get_roption('back_widget2')=='false'){
unset($wp_meta_boxes['dashboard']['normal']['core']['dashboard_recent_comments']);
}
if(get_roption('back_widget3')=='false'){
unset($wp_meta_boxes['dashboard']['normal']['core']['dashboard_incoming_links']);
}
if(get_roption('back_widget4')=='false'){
unset($wp_meta_boxes['dashboard']['side']['core']['dashboard_primary']);
}
if(get_roption('back_widget5')=='false'){
unset($wp_meta_boxes['dashboard']['side']['core']['dashboard_secondary']);
}
if(get_roption('back_widget6')=='false'){
unset($wp_meta_boxes['dashboard']['normal']['core']['dashboard_plugins']);
}
if(get_roption('back_widget7')=='false'){
unset($wp_meta_boxes['dashboard']['side']['core']['dashboard_quick_press']);
}
if(get_roption('back_widget8')=='false'){
unset($wp_meta_boxes['dashboard']['side']['core']['dashboard_recent_drafts']);
}
}
add_action('wp_dashboard_setup', 'rch_remove_dashboard_widgets' );

	
// Убираем пункты меню
function rch_remove_menus()
{
global $rst_option;
global $menu;

$r=0;

// Массив разделов меню, которые мы планируем удалить
$restricted = array();
if(get_roption('back_menu1') == 'false'){
$restricted[] = __('Dashboard');
}
if(get_roption('back_menu2') == 'false'){
$restricted[] = __('Posts');
}
if(get_roption('back_menu3') == 'false'){
$restricted[] = __('Media');
}
if(get_roption('back_menu4') == 'false'){
$restricted[] = __('Links');
}
if(get_roption('back_menu5') == 'false'){
$restricted[] = __('Pages');
}
if(get_roption('back_menu6') == 'false'){
$restricted[] = __('Comments');
}
if(get_roption('back_menu7') == 'false'){
$restricted[] = __('Plugins');
}
if(get_roption('back_menu8') == 'false'){
$restricted[] = __('Users');
}
if(get_roption('back_menu9') == 'false'){
$restricted[] = __('Tools');
}
if(get_roption('back_menu10') == 'false'){
$restricted[] = __('Settings');
}

end ($menu);
 
while (prev($menu))
{
$value = explode(' ',$menu[key($menu)][0]);
if(in_array($value[0] != NULL?$value[0]:"" , $restricted))
{
unset($menu[key($menu)]);
}
}
}
add_action('admin_menu', 'rch_remove_menus');
	
?>