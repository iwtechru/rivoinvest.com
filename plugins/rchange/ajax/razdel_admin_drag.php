<?php 
if ( 'POST' != $_SERVER['REQUEST_METHOD'] ) {
	header('Allow: POST');
	header('HTTP/1.1 405 Method Not Allowed');
	header('Content-Type: text/plain');
	exit;
}

include_once('../../../../wp-config.php');
include_once('../../../../wp-load.php');
include_once('../../../../wp-includes/wp-db.php');
header('Content-Type: text/html; charset=utf-8');

$update = $_POST['number'];

if (current_user_can('administrator')){
	$y = 0;
	foreach ($update as $ID) { $y++;

		global $wpdb;
		$wpdb->query("UPDATE ".$wpdb->prefix."rchange SET rorder='$y' WHERE id = '$ID'");
	
	}
} 
?>