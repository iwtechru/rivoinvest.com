<?php 
include_once('../../../../wp-config.php');
include_once('../../../../wp-load.php');
include_once('../../../../wp-includes/wp-db.php');
header('Content-Type: text/html; charset=utf-8');

if(current_user_can('administrator')){

global $wpdb;
$sql = "SELECT * FROM ".$wpdb->prefix."rchange ORDER BY id asc";
$exports = $wpdb->get_results($sql);


$line = '<?xml version="1.0" encoding="UTF-8"?>'."\n<channel>\r\n";

foreach($exports as $exp){
    $line .= "
	<item>\n
	    <id>{$exp->id}</id>\n
	    <rname>{$exp->rname}</rname>\n
		<rvid>{$exp->rvid}</rvid>\n
		<roption>{$exp->roption}</roption>\n
		<rdescription>{$exp->rdescription}</rdescription>\n
		<rdefault>{$exp->rdefault}</rdefault>\n
		<rslug>{$exp->rslug}</rslug>\n
		<rchto>{$exp->rchto}</rchto>\n
		<toid>{$exp->toid}</toid>\n
		<rorder>{$exp->rorder}</rorder>\n
	</item>\r\n
	";
}

$line .= "</channel>";

	$files = RCH_PLUGIN_DIR.'export.xml';
	
    $fs=@fopen($files, 'w');
    @fwrite($fs, $line);
    @fclose($fs);
	
	$files2 = RCH_PLUGIN_URL.'export.xml';

	$curl = pch_curl_parser($files2);
    if(!$curl['err']){
		$result = $curl['output'];
		header('Content-type: application/octet-stream');
        header('Content-Disposition: attachment; filename=' . $files2);
        header('Content-Length: ' . strlen($result));
           echo $result;
		   if(file_exists($files)){
		       unlink($files);
		   }
           exit(); 
	}	


} 

?>