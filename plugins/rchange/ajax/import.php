<?php 
if ( 'POST' != $_SERVER['REQUEST_METHOD'] ) {
	header('Allow: POST');
	header('HTTP/1.1 405 Method Not Allowed');
	header('Content-Type: text/plain');
	exit;
}

include_once('../../../../wp-config.php');
include_once('../../../../wp-load.php');
include_once('../../../../wp-includes/wp-db.php');
header('Content-Type: text/html; charset=utf-8');

if(current_user_can('administrator')){
$premit_ext = array(".xml");
	if($_FILES['importfile']['name']){
		$ext = strtolower(strrchr($_FILES['importfile']['name'],"."));
			if(in_array($ext,$premit_ext)){
	            if($_FILES["importfile"]["size"] > 0){
				    $tempFile = $_FILES['importfile']['tmp_name'];
					$filename = 'pch'.$_FILES['importfile']['name'];
					$filename = preg_replace("/[^A-Za-z0-9`'_\-\.]/", '-', $filename);
					$fileosn =  str_replace('//','/',RCH_PLUGIN_DIR) . $filename;
					$fileosn2 = RCH_PLUGIN_URL . $filename;
					if(move_uploaded_file($tempFile,$fileosn)){
					    $curl = pch_curl_parser($fileosn2);
						if(!$curl['err']){
                            $result = $curl['output'];
							if(strstr($result,'<?xml version="1.0" encoding="UTF-8"?>')){
							    $res = simplexml_load_string($result);
			                    $channel = $res->xpath('/channel/item');
                                if(is_array($channel)){
								    global $wpdb;
									$wpdb->query("DELETE FROM ".$wpdb->prefix."rchange");
			                        foreach($channel as $item){
									
									    $wpdb->insert( $wpdb->prefix.'rchange' , array('id'=> $item->id, 'rname'=> $item->rname, 'rvid'=> $item->rvid,'roption'=>$item->roption, 'rdescription'=> $item->rdescription, 'rdefault'=> $item->rdefault, 'rslug'=>$item->rslug,'rchto'=>$item->rchto,'toid'=>$item->toid,'rorder'=>$item->rorder)); 
				                        
			                        }
					                    $log['otv']=100; 
					                    $log['text']='Импорт успешно выполнен';									    
									    if(file_exists($fileosn)){ unlink($fileosn); }
			                    } else {
					if(file_exists($fileosn)){ unlink($fileosn); }
					$log['otv']=6; 
					$log['text']='не верный формат файла';						
						    }							
							} else {
					if(file_exists($fileosn)){ unlink($fileosn); }
					$log['otv']=6; 
					$log['text']='не верный формат файла';						
						    }
						} else {
					$log['otv']=6; 
					$log['text']='ошибка curl';						
						}
                    } else {
					$log['otv']=6; 
					$log['text']='файл не загружен';
					}
                } else {
				$log['otv']=5; 
				$log['text']='не верный размер файла';
				}					
            } else {
				$log['otv']=5; 
				$log['text']='не верный формат файла';
			}
    } else {
		$log['otv']=1; 
		$log['text']='не выбран файл';
    }
} 

$log['rchange']=1;
echo json_encode($log);
?>