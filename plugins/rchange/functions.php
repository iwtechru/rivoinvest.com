<?php
if( !defined( 'ABSPATH')){ exit(); }

function pch_curl_parser($url){
    if($ch=curl_init()){
        curl_setopt($ch,CURLOPT_URL, $url);
        curl_setopt($ch,CURLOPT_RETURNTRANSFER,1);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
        //curl_setopt($ch, CURLOPT_FOLLOWLOCATION, 1);
        curl_setopt($ch, CURLOPT_REFERER , "");
        curl_setopt($ch, CURLOPT_TIMEOUT, 25);
        curl_setopt($ch, CURLOPT_USERAGENT, "Mozilla/4.0 (compatible; MSIE 6.0; Windows NT 5.1; SV1; .NET CLR 1.1.4322)");
        $arg['output']=curl_exec($ch);
        $arg['err']=curl_errno($ch);
        curl_close($ch);
    } else {
        $arg['err']=1;
    }

return $arg;
}

function toption_array(){
global $wpdb;
$toption = $wpdb->get_results("SELECT * FROM ". $wpdb->prefix ."rchange WHERE rchto='options' AND toid != '0' ORDER BY rorder asc");
$noptionarray = array();
    foreach($toption as $top){
	    $topid = $top->toid;
        $noptionarray[$topid][]=$top;
    }
	return $noptionarray;
}

function pch_color($color){
    if(!strstr($color, '#')){
	    $color = '#'.$color;
	}
	return $color;
}

function rchange_description_form($descr){
    if(strlen($descr) > 0){ ?>
	    <div class="rdescription">
	        <?php echo $descr;?>
        </div>		
    <?php 
	} 
}

function get_admin_roption($slug){
global $rchange_option;
    return $rchange_option['rch_'.$slug];
}

function get_roption($id, $format='',$znak=','){
global $rchange_default;
    $text = get_admin_roption($id);
	if(!$text){ $text = trim($rchange_default[$id]); }

	if($format=='html' and is_string($text)){
	    $text = apply_filters('the_content',$text);
	} elseif($format=='color' and is_string($text)){
	    $text = pch_color($text);
	} elseif($format=='string' and is_array($text)){
	    $text = join($znak,$text);
	}
	
	return $text;
}

function the_roption($id, $format='', $znak=','){
    $text = get_roption($id, $format,$znak);
	    if(is_string($text)){
            echo $text;
		} 
}

function rblock_input($name, $id, $default='', $descr=''){
$znak = get_admin_roption($id);
if(!$znak){ $znak = $default; }
$znak = htmlspecialchars($znak);
?>
<div class="rblock">
	<div class="blocktitle"><?php echo $name;?></div>
		<div class="rinput">
            <input type="text" class="rlong" name="rch_<?php echo $id;?>" value="<?php echo $znak;?>" />
		</div>
	<?php rchange_description_form($descr); ?>
</div>
<?php
}

function rblock_yesno($name, $id, $descr=''){
$znak = get_admin_roption($id);
if(!$znak){ $znak = 'false'; }
?>
<div class="rblock">
	<div class="blocktitle"><?php echo $name;?></div>
	<div class="rradio">
        <label><input type="radio" name="rch_<?php echo $id;?>" value="true" <?php if($znak=='true'){?>checked="checked"<?php } ?> /> Да</label>
        <label><input type="radio" name="rch_<?php echo $id;?>" value="false" <?php if($znak=='false'){?>checked="checked"<?php } ?> /> Нет</label>
	</div>
	<?php rchange_description_form($descr); ?>
</div>
<?php
}

function rblock_radio($name, $id, $array=array(), $default='', $descr=''){
$znak = get_admin_roption($id);
if(!$znak){ $znak = $default; }
?>
<div class="rblock">
	<div class="blocktitle"><?php echo $name;?></div>
	<div class="rradio">
	    <?php if(is_array($array)){ foreach($array as $narr){ $arr = explode('*|*',$narr); ?>
        <label><input type="radio" name="rch_<?php echo $id;?>" value="<?php echo $arr[0];?>" <?php if($znak==$arr[0]){?>checked="checked"<?php } ?> /> <?php echo $arr[1];?></label>
		<?php }} ?>
	</div>
	<?php rchange_description_form($descr); ?>	
</div>
<?php
}

function rblock_checkbox($name, $id, $array=array(), $default='', $descr=''){
$znak = get_admin_roption($id);
if(!$znak){ $znak = $default; }
?>
<div class="rblock">
	<div class="blocktitle"><?php echo $name;?></div>
		<div class="rcheckbox">
		    <?php if(is_array($array)){ foreach($array as $narr){ $arr = explode('*|*',$narr); ?>
            <label><input type="checkbox" name="rch_<?php echo $id;?>[<?php echo $arr[0];?>]" value="<?php echo $arr[0];?>" <?php if($znak[$arr[0]]==$arr[0]){?>checked="checked"<?php } ?> /> <?php echo $arr[1];?></label>
			<?php }} ?>
		</div>
	<?php rchange_description_form($descr); ?>						
</div>
<?php
}

function rblock_select($name, $id, $array=array(), $default='', $descr=''){
$znak = get_admin_roption($id);
if(!$znak){ $znak = $default; }
?>
<div class="rblock">
	<div class="blocktitle"><?php echo $name;?></div>
		<div class="rselect">
		        <select name="rch_<?php echo $id;?>">
		    <?php if(is_array($array)){ foreach($array as $narr){ $arr = explode('*|*',$narr); ?>
      
					<option value="<?php echo $arr[0];?>" <?php if($znak==$arr[0]){?>selected="selected"<?php } ?>><?php echo $arr[1];?></option>
									
			<?php }} ?>
			    </select>
		</div>
	<?php rchange_description_form($descr); ?>						
</div>
<?php
}

function rblock_upload($name, $id, $default='', $descr=''){
$znak = get_admin_roption($id);
if(!$znak){ $znak = $default; }
?>
<div class="rblock">
	<div class="blocktitle"><?php echo $name;?></div>
		<div class="rupload">
            <p><input type="text" class="rlong" id="rx_<?php echo $id;?>" name="rch_<?php echo $id;?>" value="<?php echo $znak;?>" /></p>
			<p class="submit uploadx"><input type="button" id="rx_<?php echo $id;?>_button" name="" class="upload_image_button" value="Загрузить" /></p>
		</div>
	<?php rchange_description_form($descr); ?>						
</div>
<?php
}	

function rblock_cdate($name, $id, $default='', $descr=''){
$znak = get_admin_roption($id);
?>
<div class="rblock">
	<div class="blocktitle"><?php echo $name;?></div>
		<div class="rcolorpicker">
            <input type="text" class="rlong rcdate" name="rch_<?php echo $id;?>" value="<?php echo $znak;?>" />
		</div>
	<?php rchange_description_form($descr); ?>						
</div>
<?php
}

function rblock_cdatetime($name, $id, $default='', $descr=''){
$znak = get_admin_roption($id);
?>
<div class="rblock">
	<div class="blocktitle"><?php echo $name;?></div>
		<div class="rcolorpicker">
            <input type="text" class="rlong rcdatetime" name="rch_<?php echo $id;?>" value="<?php echo $znak;?>" />
		</div>
	<?php rchange_description_form($descr); ?>						
</div>
<?php
}

function rblock_color($name, $id, $default='', $descr=''){
$znak = get_admin_roption($id);
if(!$znak){ $znak = $default; }
if(!$znak){ $znak = '#ffffff'; }
$znak = pch_color($znak);
?>
<div class="rblock">
	<div class="blocktitle"><?php echo $name;?>  <div class="primercolor" style="background: <?php echo $znak;?>;"></div></div>
		<div class="rcolorpicker">
            <input type="text" class="rlong rthecolor" name="rch_<?php echo $id;?>" value="<?php echo $znak;?>" />
		</div>
	<?php rchange_description_form($descr); ?>						
</div>
<?php
}

function rblock_textarea($name, $id, $default='', $descr=''){
$znak = get_admin_roption($id);
if(!$znak){ $znak = $default; }
?>
<div class="rblock">
	<div class="blocktitle"><?php echo $name;?></div>
		<div class="rtextarea">
            <textarea name="rch_<?php echo $id;?>" style="height: 100px;"><?php echo $znak;?></textarea>
		</div>
		<?php rchange_description_form($descr); ?>						
</div>
<?php
}

function rch_remove_admin_bar_links() {
    global $wp_admin_bar;
	
    if(current_user_can('administrator')){
	
    $defaults = array(
	'title' => 'Настройки',
	'href' => admin_url('admin.php?page=rchange/option.php'),
	'parent' => 'appearance', 
	'id' => 'rch_config'
    );
	$wp_admin_bar->add_menu($defaults);	
	
	}
	
}
add_action('wp_before_admin_bar_render', 'rch_remove_admin_bar_links');

function rch_del_quote($text){
    return htmlspecialchars(str_replace("'",'',$text));
}

function rch_del_quoter($text){
    return trim(stripslashes(str_replace(array("'"),'',$text)));
}

?>