<?php
add_theme_support('menus');
register_nav_menu('hmenu', 'Горизонтальное меню');
add_theme_support( 'post-thumbnails' );
set_post_thumbnail_size(800, 900, false);
add_image_size('thumb159', 159,159,true); // Характерное изображение в контенте
function pchange($change=''){
	if($change and function_exists('get_roption')){
		return get_roption($change);
	}
}
/*
function filter_search($query) {
	if ($query->is_search) {
		$query->set('post_type', array('post','page', 'services'));
	};
	return $query;
};
add_filter('pre_get_posts', 'filter_search');
*/
class MFC_Walker_Nav_Menu extends Walker_Nav_Menu {
	function start_el(&$output, $item, $depth, $args) {
		global $wp_query;           
		$indent = ( $depth ) ? str_repeat( "\t", $depth ) : '';
		$class_names = $value = '';
		$classes = empty( $item->classes ) ? array() : (array) $item->classes;
		$classes[] = 'menu-item-' . $item->ID;
		$classes[]='nav-item';
		$class_names = join( ' ', apply_filters( 'nav_menu_css_class', array_filter( $classes ), $item, $args ) );
		$class_names = ' class="' . esc_attr( $class_names ) . '"';
		$id = apply_filters( 'nav_menu_item_id', 'menu-item-'. $item->ID, $item, $args );
		$id = strlen( $id ) ? ' id="' . esc_attr( $id ) . '"' : '';
		$attributes  = ! empty( $item->attr_title ) ? ' title="'  . esc_attr( $item->attr_title ) .'"' : '';
		$attributes .= ! empty( $item->target )     ? ' target="' . esc_attr( $item->target     ) .'"' : '';
		$attributes .= ! empty( $item->xfn )        ? ' rel="'    . esc_attr( $item->xfn        ) .'"' : '';
		$attributes .= ! empty( $item->url )        ? ' href="'   . esc_attr( $item->url        ) .'"' : '';
		if(in_array('current-menu-item', $classes)) {
			$attributes .= ' class="active"';
		}
		else {
			$attributes.=' class=""';
		}
		$fields=get_post_custom((int)$item->object_id); 
		$img = $fields['menuimg'][0];
		$pic = wp_get_attachment_image_src($img,'original');
		if ($depth ==0){
			$item_output.='<div class="l-jcol">';
			$attributes = str_replace('class="', 'class="nav-item ', $attributes);
			$item_output .= '<a'. $attributes .'>';
			$item_output.='<div class="nav-pic">';
			$item_output.='<img src="'.$pic[0].'" alt="'.$item->attr_title.'"/>';
			$item_output.='</div>';
			$item_output.='<div class="nav-text">';		
			$item_output.='<u>'."\n\r";
			$item_output.='';
			$item_output .= $args->link_before . apply_filters( 'the_title', $item->title, $item->ID ) . $args->link_after;
			$item_output.='</u></div>';
			$item_output .= '</a>';
			$item_output.='</div>';
		} else {
			$item_output .= '<a'. $attributes .'>';
			$item_output .= $args->link_before . apply_filters( 'the_title', $item->title, $item->ID ) . $args->link_after;
			$item_output.='</a>';
		}
		$output .= apply_filters( 'walker_nav_menu_start_el', $item_output, $item, $depth, $args );

	}
}
$args = array(
	'name'          => __( 'Sidebar name', 'theme_text_domain' ),
	'id'            => 'unique-sidebar-id',
	'description'   => '',
	'class'         => '',
	'before_widget' => '<li id="%1$s" class="widget %2$s">',
	'after_widget'  => '</li>',
	'before_title'  => '<h2 class="widgettitle">',
	'after_title'   => '</h2>' ); 
register_sidebar( $args );

function my_attachments( $attachments )
{
	$fields         = array(
		array(
      'name'      => 'title',                         // unique field name
      'type'      => 'text',                          // registered field type
      'label'     => __( 'Title', 'attachments' ),    // label to display
      'default'   => 'title',                         // default value upon selection
      ),
		array(
      'name'      => 'caption',                       // unique field name
      'type'      => 'textarea',                      // registered field type
      'label'     => __( 'Caption', 'attachments' ),  // label to display
      'default'   => 'caption',                       // default value upon selection
      ),
		);

	$args = array(    
		'label'         => 'Attachments',
		'post_type'     => array( 'works', 'services' ),
		'position'      => 'normal',
		'priority'      => 'high',
		'filetype'      => null,  
		'note'          => 'Attach files here!',
		'append'        => true,
		'button_text'   => __( 'Attach Files', 'attachments' ),
		'modal_text'    => __( 'Attach', 'attachments' ),
		'router'        => 'browse',
		'fields'        => $fields,
		);
  $attachments->register( 'attachments', $args ); // unique instance name
}
add_action( 'attachments_register', 'my_attachments' );
function iwt_get_att_by_url($url){ // Получение всех данных о вложении по ссылке.
	global $wpdb;
	$url = str_replace(site_url('/wp-content/uploads/'),'', $url);
	$query = 'SELECT post_id FROM `'.$wpdb->prefix.'postmeta` WHERE meta_value LIKE \'%'.$url.'%\' AND meta_key ="_wp_attached_file"';
	$pid = $wpdb->get_var($query);
	$out['post-id'] = $pid;
	$out['alt']= get_post_meta($pid, '_wp_attachment_image_alt', true);
	$attachment = get_post($pid);		
	$out['title'] = $attachment->post_title;
	$out['caption'] = $attachment->post_excerpt;
	$out['description'] = $image->post_content;
	return $out;		
}
add_action( 'attachments_register', 'my_attachments' );
function get_template_part2($inc, $params = false){
	include(get_template_directory().'/'.$inc.'.php');
}
?>