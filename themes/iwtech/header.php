<!DOCTYPE html>
<html>
<head>
    <link type="text/css" rel="StyleSheet" href="<?php bloginfo('template_directory');?>/css/style.css"/>
    <link type="text/css" rel="StyleSheet" href="<?php bloginfo('template_directory');?>/css/normalize.css"/>
    <link type="text/css" rel="StyleSheet" href="<?php bloginfo('template_directory');?>/css/formalize.css"/>
    <link type="text/css" rel="StyleSheet" href="<?php bloginfo('template_directory');?>/css/iwt-corr.css"/>
    <link href='http://fonts.googleapis.com/css?family=PT+Sans&amp;subset=latin,cyrillic' rel='stylesheet' type='text/css'>
    <meta charset="utf-8">
    <title>		<?php
    global $page, $paged;
    wp_title( '|', true, 'right' );
    bloginfo( 'name' );
    $site_description = get_bloginfo( 'description', 'display' );
    if ( $site_description && ( is_home() || is_front_page() ) )
      echo " | $site_description";
// Add a page number if necessary:
  if ( $paged >= 2 || $page >= 2 )
      echo ' | ' . sprintf( __( 'Page %s', 'iwtech' ), max( $paged, $page ) );?>
</title>
<?php wp_head();?>
</head>
<body>
    <div class="top_line">
        <div class="width_K">
            <p>Проект работает: 10 дн.</p>

            <p>Всего участников: 121</p>

            <p>Принято средств: 5800$</p>

            <p>Выплачено средств: 1900$</p>
            <ul class="floatright">
                <li><a href="<?php bloginfo('template_directory');?>/#" id="rus-lang"></a></li>
                <li><a href="<?php bloginfo('template_directory');?>/#" id="usa-lang"></a></li>
                <li><a href="<?php bloginfo('template_directory');?>/#" id="brd-lang"></a></li>
            </ul>
        </div>
        <div class="clearfloat"></div>
    </div>
    <!--end #top_line -->
    <div class="width_K">
        <header>
            <a href="<?php echo home_url();?>" id="logo" class="floatleft"><img src="<?php bloginfo('template_directory');?>/images/logo.png"></a>

          <?php get_template_part('header','welcome');?>
        </header>
        <!--end header -->

        <div id="wrapper">
           