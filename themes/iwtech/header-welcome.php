<?php
if (!is_user_logged_in()) {    
   ?>
   <div class="login-status ofline floatleft">
       <?php 
       if(1==1){
        $args = array(
            'echo'=>false,
            'redirect'=>home_url(),
            'label_username' =>false,
            'label_password' =>false,
            'label_remember' =>false,
            'remember' =>false
            );        
        $login_form = wp_login_form( $args );
        $login_form = preg_replace('/<\/*p[^\>]*\>/','',$login_form);
        $login_form = preg_replace('/<\/*label[^>]*>/','', $login_form);
        $login_form = preg_replace('/size\=\"[^\"]*\"/','', $login_form);
        ?><img src="<?php bloginfo('template_directory');?>/images/lock.png" id="iwt-lock"/><?php
        echo $login_form;
    } else { ?> 
    <div id="login" class="floatleft"><input placeholder="e-mail" name="login"></div>
    <img src="<?php bloginfo('template_directory');?>/images/lock.png" id="iwt-lock"/>

    <div id="password" class="floatright"><input placeholder="пароль" name="password"></div>
    <?php } ?>
    <a href="<?php echo get_permalink(51);?>">Регистрация</a>
    <a href="<?php echo get_permalink(67);?>">Забыли пароль?</a>
    <a href="#" id="loginbutton" class="padding-none">Войти</a>
    <script type="text/javascript" src="<?php bloginfo('template_directory');?>/js/iwt-login.js"></script>
</div>
<?php } else {
    global $current_user;
    get_currentuserinfo();
    $data =get_user_meta($current_user->ID);
    if(!$current_user->user_firstname){
        wp_update_user( array ( 'ID' => $current_user->ID, 'first_name' => $data['firstname'][0] ) ) ;
    }
    if(!$current_user->user_lastname){
        wp_update_user( array ( 'ID' => $current_user->ID, 'last_name' => $data['lastname'][0] ) ) ;
    } 
    ?>
    <div class="login-status online floatleft">
        <p id="hello_user">Здравствуйте, <span><?php echo $current_user->user_firstname.' '.$current_user->user_lastname;?></span></p>
        <a href="<?php echo get_permalink(79);?>" id="return_offise">Вернуться в кабинет</a>
        <a href="<?php echo wp_logout_url( home_url() ); ?>" id="exit_offise">Выйти из кабинета</a>
    </div>
    <?php } ?>
    <div class="clearfloat"></div>