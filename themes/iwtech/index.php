<?php get_header();?>
 <?php get_template_part('navigation','left');?>
<div id="content-blok" class="floatleft staticpage">
	<div id="content-wrap">
		<?php get_template_part('block','action123');?>
		<div id="kontent">
			<div id="leftblok" class="floatleft">
				<h2><?php the_title();?></h2>

				<?php
				if ( have_posts() ) {
					while ( have_posts() ) { the_post(); 
						get_template_part('content',get_post_type());
					}
				}
				?>

			</div>

			<div class="clearfloat"></div>
		</div>
		<!--end #kontent -->
	</div>
</div>
<!--========================== BEGIN =================================== -->
<div class="clearfloat"></div>
<!--========================== END =================================== -->
</div>  
<?php get_footer();?>