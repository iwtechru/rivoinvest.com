<?php
/*
Template name: Вопросы и ответы
*/
?>
<?php get_header();?>
 <?php get_template_part('navigation','left');?>
<div id="content-blok" class="floatleft">
	<div id="content-wrap">
		<div id="kontent" class="faq">
			<h2><?php the_title();?></h2>
			<div id="faq_p">
				<?php
				if ( have_posts() ) {
					while ( have_posts() ) { the_post(); 
						get_template_part('content',get_post_type());
					}
				}
				?>
			</div>
		</div>
	</div>
</div>
</div>

<script type="text/javascript" src="<?php bloginfo('template_directory');?>/js/iwt-corr-faq.js"></script>
<?php get_footer();?>