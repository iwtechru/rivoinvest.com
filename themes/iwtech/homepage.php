<?php
/*
Template name: Шаблон главной страницы
*/
?>
<?php get_header();?>
 <?php get_template_part('navigation','left');?>
<div id="content-blok" class="floatleft">
    <div id="content-wrap">
      <?php get_template_part('block','action123');?>
                    <div id="kontent">
                        <div id="leftblok" class="floatleft">
                            <h2><?php the_title();?></h2>

                            <?php
                            if ( have_posts() ) {
                                while ( have_posts() ) { the_post(); 
                                    get_template_part('content',get_post_type());
                                }
                            }
                            ?>

                            <div class="floatleft" id="money">
                                <h2>ПРИНИМАЕМ К ОПЛАТЕ:</h2>
                                <img src="<?php bloginfo('template_directory');?>/images/perfektmoney.jpg">
                            </div>
                            <div id="obmen">
                                <h2>ОБМЕННИКИ:</h2>

                                <p>www.magnetic-exchange.com</p>

                                <p>www.e-obmen.net </p>

                                <p>www.x-obmen.com</p>

                                <p>www.office4exchnge.com</p>
                            </div>
                        </div>
                        <div id="rightblok" class="floatright">
                            <?php get_template_part('block', 'news');?>
                            <div id="statistik">
                                <div class="angle"></div>
                                <p class="name">Статистика проекта</p>

                                <p class="desk">Проект работает:<span>10 дн.</span></p>

                                <p class="desk">Всего участников:<span>121</span></p>

                                <p class="desk">Сейчас на сайте: <span>15</span></p>

                                <p class="desk">Принято средств:<span>5800$</span></p>

                                <p class="desk">Выплачено средств:<span>1900$</span></p>

                                <p class="desk">Новый вклад<span>20$</span></p>

                                <p class="desk">Новый участник<span>Jjfsd</span></p>
                            </div>
                            <div id="sozial_link">
                                <div class="angle"></div>
                                <p class="name">Поделиться</p>
                                <ul>
                                    <li><a href="<?php bloginfo('template_directory');?>/#" id="facebook"></a></li>
                                    <li><a href="<?php bloginfo('template_directory');?>/#" id="e-mail"></a></li>
                                    <li><a href="<?php bloginfo('template_directory');?>/#" id="ok"></a></li>
                                    <li><a href="<?php bloginfo('template_directory');?>/#" id="twit"></a></li>
                                    <li><a href="<?php bloginfo('template_directory');?>/#" id="vk"></a></li>
                                </ul>

                            </div>
                        </div>
                        <div class="clearfloat"></div>
                    </div>
                    <!--end #kontent -->
                    <?php get_template_part('block','feedback');?>
                    <?php get_template_part('block','comments');?>
                </div>
            </div>
            <!--========================== BEGIN =================================== -->
            <div class="clearfloat"></div>
            <!--========================== END =================================== -->
        </div>  
        <?php get_footer();?>