<?php
add_theme_support('menus');
register_nav_menu('aside-nav', 'Вертикальное меню');
if ( current_user_can( 'manage_options' ) ) { } else  {
	add_filter('show_admin_bar', '__return_false');
}
set_post_thumbnail_size(900, 900, false);
add_image_size('thumb159', 159,159,true); // Характерное изображение в контенте
add_image_size('thumb848', 848, 333, true); // изображение статичных страниц
function iwt_get_the_excerpt($post_id) {
	global $post;  
	$save_post = $post;
	$post = get_post($post_id);
	$output = get_the_excerpt();
	$post = $save_post;
	return $output;
}
// Change login credentials
// remove the default filter
remove_filter( 'authenticate', 'wp_authenticate_username_password', 20, 3 );
// add custom filter
add_filter( 'authenticate', 'my_authenticate_username_password', 20, 3 );
function my_authenticate_username_password( $user, $username, $password ) {

    // If an email address is entered in the username box, 
    // then look up the matching username and authenticate as per normal, using that.
	if ( ! empty( $username ) ) {
        //if the username doesn't contain a @ set username to blank string
        //causes authenticate to fail
		if(strpos($username, '@') == FALSE){
			$username = '';
		}
		$user = get_user_by( 'email', $username );
	}
	if ( isset( $user->user_login, $user ) )
		$username = $user->user_login;

    // using the username found when looking up via email
	return wp_authenticate_username_password( NULL, $username, $password );
} 
if( !function_exists(retrieve_password1)){
    function retrieve_password1($user_email) {
     global $wpdb, $current_site;

     $errors = new WP_Error();

        // redefining user_login ensures we return the right case in the email
     $user_login = $user_email;

        do_action('retreive_password', $user_login);  // Misspelled and deprecated
        do_action('retrieve_password', $user_login);



        $key = $wpdb->get_var($wpdb->prepare("SELECT user_activation_key FROM $wpdb->users WHERE user_login = %s", $user_login));
        if ( empty($key) ) {
                // Generate something random for a key...
        	$key = wp_generate_password(20, false);
        	do_action('retrieve_password_key', $user_login, $key);
                // Now insert the new md5 key into the db
        	$wpdb->update($wpdb->users, array('user_activation_key' => $key), array('user_login' => $user_login));
        }
        $message = __('Someone requested that the password be reset for the following account:') . "\r\n\r\n";
        $message .= network_site_url() . "\r\n\r\n";
        $message .= sprintf(__('Username: %s'), $user_login) . "\r\n\r\n";
        $message .= __('If this was a mistake, just ignore this email and nothing will happen.') . "\r\n\r\n";
        $message .= __('To reset your password, visit the following address:') . "\r\n\r\n";
        $message .= '<' . network_site_url("wp-login.php?action=rp&key=$key&login=" . rawurlencode($user_login), 'login') . ">\r\n";

        if ( is_multisite() )
        	$blogname = $GLOBALS['current_site']->site_name;
        else
                // The blogname option is escaped with esc_html on the way into the database in sanitize_option
                // we want to reverse this for the plain text arena of emails.
        	$blogname = wp_specialchars_decode(get_option('blogname'), ENT_QUOTES);

        $title = sprintf( __('[%s] Password Reset'), $blogname );

        $title = apply_filters('retrieve_password_title', $title);
        $message = apply_filters('retrieve_password_message', $message, $key);

        if ( $message && !wp_mail($user_email, $title, $message) )
        	wp_die( __('The e-mail could not be sent.') . "<br />\n" . __('Possible reason: your host may have disabled the mail() function...') );

        return true;
    }
}
add_action( 'wp_login_failed', 'my_front_end_login_fail' );  // hook failed login

function my_front_end_login_fail( $username ) {
   $referrer = $_SERVER['HTTP_REFERER'];  // where did the post submission come from?
   // if there's a valid referrer, and it's not the default log-in screen
   if ( !empty($referrer) && !strstr($referrer,'wp-login') && !strstr($referrer,'wp-admin') ) {
      wp_redirect( $referrer . '?login=failed' );  // let's append some information (login=failed) to the URL for the theme to use
      exit;
  }
}
?>