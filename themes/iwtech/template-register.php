<?php
/*
Template name: Регистрация
*/
?>
<?php if (is_user_logged_in()){
	wp_redirect( home_url() ); exit; 
} else {
	get_header();?>
	<div id="content-blok" class="floatleft">
		<div id="content-wrap" class="faq">
			<h2><?php the_title();?></h2>
			<div id="faq_p">
				<?php
				if ( have_posts() ) {
					while ( have_posts() ) { the_post(); 
						get_template_part('content',get_post_type());
					}
				}
				?>
			</div>
		</div>
	</div>
</div>

<script type="text/javascript" src="<?php bloginfo('template_directory');?>/js/iwt-corr-reg.js"></script>
<?php get_footer();?>
<?php } ?>