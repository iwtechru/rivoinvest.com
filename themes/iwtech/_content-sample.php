<?php
if ( have_posts() ) {
	while ( have_posts() ) { the_post(); 
		get_template_part('content','contacts');
	}
}
?>
<?php $attachments = attachments_get_attachments(); 
if ($attachments){
	?>
	<ul>
		<?php 
		foreach ($attachments as $att){ 
			$fornbox =wp_get_attachment_image_src($att['id'],'thumb650');  
			$thumb =wp_get_attachment_image_src($att['id'],'thumb242' ); 
			?>
			<li>
				<a href="<?php echo $fornbox[0];?>" class="n-box">
					<img src="<?php echo $thumb[0];?>" alt=""/>
				</a>
			</li>
			<?php } ?>
		</ul>
		<?php } ?>
		<?php $fields=get_post_custom();  ?>

		<?php 
		$args = array( 'post_type' => 'page', 
			'posts_per_page' => 3, 
			'meta_key'=>'порядок_отображения_на_главной',  
			'orderby' => 'meta_value_num', 
			'order' => ASC );
		$args['meta_query'][] = array(
			'key'=>'порядок_отображения_на_главной',
			'value'=>0,
			'compare'=>'>='
			);
		$articles = get_posts($args);
		?>
