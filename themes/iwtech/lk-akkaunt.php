<?php
/*
Template name: Кабинет - личные данные
*/
?>
<?php get_header();?>
<?php get_template_part('navigation','left-lk');?>
<?php get_template_part('navigation','top-lk');?>
<div id="content-blok" class="floatleft">
	<div id="content-wrap" class="akkaunt">
		<h3>Аккаунт</h3>
		<div class="akkaunt-info">
			<div class="akkaunt-line"><p class="floatleft">Ваш логин(e-mail):</p><input placeholder="ivanov_ivan@gmail.com"></div>
			<div class="akkaunt-line"><p class="floatleft">Ваше имя пользователя на сайте:</p><input placeholder="Иван"></div>
			<div class="akkaunt-line"><p class="floatleft">Дата регистрации:</p><input placeholder="10:30 21-03-2013"></div>
			<div class="akkaunt-line"><p class="floatleft">Реферальная ссылка:</p><input placeholder="http://rivoinvest.com/256"></div>
			<div class="akkaunt-line"><p class="floatleft">Ваша платежная система:</p><img src="./images/PM.png"><span id="pm-orang">U5026838</span><p id="pm_desk">На данную платежную систему будут производиться все выплаты</p> </div>
		</div>
		<h3>Вклады</h3>
		<div class="akkaunt-info">
			<div class="akkaunt-line"><p class="floatleft">Активные вклады</p><input placeholder="2 на сумму 1500 $"></div>
			<div class="akkaunt-line"><p class="floatleft">Ожидаемая сумма выплат</p><input placeholder="2700 $"></div>
			<div class="akkaunt-line"><p class="floatleft">Выплаты по вкладам</p><input placeholder="1000 $"></div>
		</div>
		<h3>Статистика по партнерской программе</h3>
		<div class="akkaunt-info text-border">
			<div><div id="check" class="floatleft"></div>
			<p class="proz_s_part">8% с каждого партнера</p>
			<p class="kol-vo_part">Партнеров: 2</p>
			<p class="summa_vklad">Сумма вкладов: 2000 $</p>
			<p class="viplacheno">Выплачено: 160.00 $</p></div>
			<div>
				<div id="error" class="floatleft"></div>
				<p class="border-bottom-none proz_s_part">20% с каждого 10го партнера</p>
				<p class="border-bottom-none kol-vo_part">Партнеров: 2</p>
				<p class="border-bottom-none summa_vklad">Сумма вкладов: 2000 $</p>
				<p class="border-bottom-none viplacheno">Выплачено: 160.00 $</p></div>
				<div class="clearfloat"></div>

			</div>
			<div id="tie" class="floatleft"></div><p class="floatleft mini-stat">Всего партнеров: 2</p>
			<div id="dollar" class="floatleft"></div><p class="mini-stat">Брнусов выплачено: 160.00 $</p>
			<div class="clearfloat"></div>
		</div>
	</div>
	<div class="clearfloat"></div>
</div>
<?php get_footer();?>